<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Set up database table for shops
 */
final class Version20211227230834 extends AbstractMigration
{
    /**
     * @inheritDoc
     */
    public function getDescription(): string
    {
        return 'Set up database table for shops';
    }

    /**
     * @inheritDoc
     */
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE shop (id INT AUTO_INCREMENT NOT NULL, shop_id VARCHAR(255) NOT NULL, shop_url VARCHAR(255) NOT NULL, shop_secret VARCHAR(255) NOT NULL, api_key VARCHAR(255) DEFAULT NULL, secret_key VARCHAR(255) DEFAULT NULL, active TINYINT(1) DEFAULT \'0\' NOT NULL, UNIQUE INDEX UNIQ_AC6A4CA24D16C4DD (shop_id), UNIQUE INDEX UNIQ_AC6A4CA2678D9590 (shop_url), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    /**
     * @inheritDoc
     */
    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE shop');
    }
}
