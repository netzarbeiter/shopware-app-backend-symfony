<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Controller handling app registration with Shopware
 *
 * @Route(path="/app")
 */
class RegistrationController extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController
{
    /**
     * Shop repository
     *
     * @var \App\Repository\ShopRepository
     */
    protected $shopRepository;

    /**
     * RegistrationController constructor.
     *
     * @param \App\Repository\ShopRepository $shopRepository
     */
    public function __construct(\App\Repository\ShopRepository $shopRepository)
    {
        $this->shopRepository = $shopRepository;
    }

    /**
     * Register app.
     *
     * @Route(name="app.register", path="/register", methods={"GET"})
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function register(Request $request): JsonResponse
    {
        // Validate request.
        // @see https://symfony.com/doc/current/form/without_class.html and https://symfony.com/doc/current/form/direct_submit.html
        $form = $this->createFormBuilder()
            ->add('shop-id', TextType::class, ['constraints' => [new NotBlank()]])
            ->add('shop-url', TextType::class, ['constraints' => [new NotBlank()]])
            ->add('timestamp', IntegerType::class, ['constraints' => [new NotBlank()]])
            ->getForm();
        $form->submit($request->query->all());
        if (!$form->isValid()) {
            return $this->json(['error' => 'Insufficient request parameters'], 400);
        }
        $data = $form->getData();

        // Check signature.
        $signature = hash_hmac('sha256', urldecode($request->getQueryString()), $this->getParameter('shopware.app.secret'));
        if ($request->headers->get('shopware-app-signature') !== $signature) {
            return $this->json(['error' => 'App signature invalid'], 400);
        }

        // Get shop by shop id.
        $shop = $this->shopRepository->findOneBy(['shopId' => $data['shop-id']]);
        if (!$shop) {
            $shop = (new \App\Entity\Shop())
                ->setShopId($data['shop-id']);
        }

        // Update and save shop.
        $shop->setShopUrl($data['shop-url'])
            ->setShopSecret(
                preg_replace_callback(
                    '/\*/u',
                    function (): string {
                        switch (mt_rand(0, 2)) {
                            // Number
                            case 0:
                                return chr(mt_rand(48, 57));

                            // Uppercase letter
                            case 1:
                                return chr(mt_rand(65, 90));

                            // Lowercase letter
                            case 2:
                                return chr(mt_rand(97, 122));
                        }
                    },
                    str_repeat('*', 128)
                )
            )
            ->setActive(false);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($shop);
        $entityManager->flush();

        // Send final response.
        return $this->json(
            [
                'proof' => hash_hmac(
                    'sha256',
                    $data['shop-id'] . $data['shop-url'] . $this->getParameter('shopware.app.name'),
                    $this->getParameter('shopware.app.secret')
                ),
                'secret' => $shop->getShopSecret(),
                'confirmation_url' => $this->generateUrl('app.confirm', [], \Symfony\Component\Routing\Generator\UrlGeneratorInterface::ABSOLUTE_URL),
            ]
        );
    }

    /**
     * Confirm app registration.
     *
     * @Route(name="app.confirm", path="/confirm", methods={"POST"})
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function confirm(Request $request): JsonResponse
    {
        // Get request body.
        $data = $request->toArray();

        // Get shop by shop id and url.
        $shop = $this->shopRepository->findOneBy(['shopId' => $data['shopId'], 'shopUrl' => $data['shopUrl']]);
        if (!$shop) {
            return $this->json(['error' => 'Shop not found'], 400);
        }

        // Check signature.
        $signature = hash_hmac('sha256', $request->getContent(), $shop->getShopSecret());
        if ($request->headers->get('shopware-shop-signature') !== $signature) {
            return $this->json(['error' => 'Shop signature invalid'], 400);
        }

        // Save API and secret key.
        $shop
            ->setApiKey($data['apiKey'])
            ->setSecretKey($data['secretKey']);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($shop);
        $entityManager->flush();

        // Return empty response as sign of success (there's nothing documented in the Shopware docs what to do here).
        return $this->json(null);
    }
}
