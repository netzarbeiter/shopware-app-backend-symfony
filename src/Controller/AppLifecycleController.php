<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller dealing with webhooks for app lifecycle events
 *
 * @Route(path="/webhook/app")
 */
class AppLifecycleController extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController
{

    /**
     * Handle the 'app.installed' webhook.
     *
     * @Route(name="webhook.app.installed", path="/installed", methods={"GET", "POST"})
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function installed(Request $request): JsonResponse
    {
        // @todo Add some meaningful code

        return $this->json(null);
    }

    /**
     * Handle the 'app.updated' webhook.
     *
     * @Route(name="webhook.app.updated", path="/updated", methods={"GET", "POST"})
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function updated(Request $request): JsonResponse
    {
        // @todo Add some meaningful code

        return $this->json(null);
    }

    /**
     * Handle the 'app.deleted' webhook.
     *
     * @Route(name="webhook.app.deleted", path="/deleted", methods={"GET", "POST"})
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function deleted(Request $request): JsonResponse
    {
        // Delete shop from the database.
        /** @var \App\Entity\Shop $shop */
        $shop = $request->attributes->get('shop');
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($shop);
        $entityManager->flush();

        return $this->json(null);
    }

    /**
     * Handle the 'app.activated' webhook.
     *
     * @Route(name="webhook.app.activated", path="/activated", methods={"GET", "POST"})
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function activated(Request $request): JsonResponse
    {
        // Activate shop.
        /** @var \App\Entity\Shop $shop */
        $shop = $request->attributes->get('shop');
        $shop->setActive(true);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($shop);
        $entityManager->flush();

        return $this->json(null);
    }

    /**
     * Handle the 'app.deactivated' webhook.
     *
     * @Route(name="webhook.app.deactivated", path="/deactivated", methods={"GET", "POST"})
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function deactivated(Request $request): JsonResponse
    {
        // Activate shop.
        /** @var \App\Entity\Shop $shop */
        $shop = $request->attributes->get('shop');
        $shop->setActive(false);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($shop);
        $entityManager->flush();

        return $this->json(null);
    }
}
