<?php

declare(strict_types=1);

namespace App\Subscriber;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Subscriber to validate webhook requests
 */
class ValidateWebhookSubscriber implements \Symfony\Component\EventDispatcher\EventSubscriberInterface
{
    /**
     * Shop repository
     *
     * @var \App\Repository\ShopRepository
     */
    protected $shopRepository;

    /**
     * ValidateWebhookSubscriber constructor.
     *
     * @param \App\Repository\ShopRepository $shopRepository
     */
    public function __construct(\App\Repository\ShopRepository $shopRepository)
    {
        $this->shopRepository = $shopRepository;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            'kernel.request' => 'validateWebhook',
        ];
    }

    public function validateWebhook(\Symfony\Component\HttpKernel\Event\RequestEvent $event): void
    {
        // Get shortcut to request.
        $request = $event->getRequest();

        // Check if we have a webhook route.
        if (!preg_match('~^/webhook/~', $request->getPathInfo())) {
            return;
        }

        // Get shop by shop id and url.
        try {
            $data = $request->toArray();
        } catch (\Symfony\Component\HttpFoundation\Exception\JsonException $e) {
            $event->setResponse(new JsonResponse(['error' => 'Webhook payload missing'], 400));

            return;
        }
        $shop = $this->shopRepository->findOneBy(['shopId' => $data['source']['shopId']]);
        if (!$shop) {
            $event->setResponse(new JsonResponse(['error' => 'Shop not found'], 400));

            return;
        }

        // Check signature.
        $signature = hash_hmac('sha256', $request->getContent(), $shop->getShopSecret());
        if ($request->headers->get('shopware-shop-signature') !== $signature) {
            $event->setResponse(new JsonResponse(['error' => 'Shop signature invalid'], 400));

            return;
        }

        // Extend request with shop to make further processing easier.
        $request->attributes->set('shop', $shop);
    }
}
